use clap::{App, Arg};
use csv;
use serde::{Deserialize, Serialize};
use std::error::Error;

#[derive(Debug, Deserialize, Serialize)]
struct EmployeeRecord {
    #[serde(rename = "employee_id")]
    employee_id: u32,
    #[serde(rename = "name")]
    name: String,
    #[serde(rename = "department")]
    department: String,
    #[serde(rename = "salary")]
    salary: u32,
    #[serde(rename = "yearswithcompany")]
    years_with_company: u8, 
}


#[tokio::main]
async fn main() {
    let matches = App::new("Employee CLI Tool")
        .version("0.1.0")
        .author("XYZ")
        .about("A command-line tool for processing employee CSV files")
        .arg(Arg::with_name("file")
            .help("Sets the input CSV file to use")
            .required(true)
            .index(1))
        .arg(Arg::with_name("department")
            .help("Sets the department to filter employees by")
            .short("d")
            .long("department")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("min_salary")
            .help("Sets the minimum salary for the filter")
            .short("n") 
            .long("min_salary")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("max_salary")
            .help("Sets the maximum salary for the filter")
            .short("m")
            .long("max_salary")
            .takes_value(true)
            .required(true))
        .get_matches();

    let file_path = matches.value_of("file").unwrap();
    let department = matches.value_of("department").unwrap();
    let min_salary = matches.value_of("min_salary").unwrap();
    let max_salary = matches.value_of("max_salary").unwrap();

    match process_csv(file_path) {
        Ok(records) => {
            match filter_records(records, department, min_salary, max_salary) {
                Ok(filtered_records) => print_records(filtered_records),
                Err(e) => eprintln!("Error filtering records: {}", e),
            }
        },
        Err(e) => eprintln!("Failed to process CSV file: {}", e),
    }
}

fn process_csv(file_path: &str) -> Result<Vec<EmployeeRecord>, Box<dyn Error>> {
    let mut reader = csv::Reader::from_path(file_path)?;
    let records = reader.deserialize()
        .filter_map(Result::ok)
        .collect();
    Ok(records)
}

fn filter_records(records: Vec<EmployeeRecord>, department: &str, min_salary: &str, max_salary: &str) -> Result<Vec<EmployeeRecord>, Box<dyn Error>> {
    let min_salary: u32 = min_salary.parse()?;
    let max_salary: u32 = max_salary.parse()?;
    let filtered_records = records.into_iter().filter(|record| {
        record.department == department && record.salary >= min_salary && record.salary <= max_salary
    }).collect();
    Ok(filtered_records)
}

fn print_records(records: Vec<EmployeeRecord>) {
    if records.is_empty() {
        println!("No records found with the specified filters.");
    } else {
        for record in records {
            println!("{:?}", record);
        }
    }
}




#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_salary_filter() {
        // Assuming the path is correct and accessible during tests
        // You might need to adjust the path based on your test environment setup
        let file_path = "data/employees.csv"; 
        let department = "Finance";
        let min_salary = "70000";
        let max_salary = "90000";

        // Since the process_csv and filter_records functions now properly handle errors,
        // and return results, we need to adjust the unwrapping here.
        let records = process_csv(file_path).expect("Failed to process CSV");
        let filtered_records = filter_records(records, department, min_salary, max_salary).expect("Failed to filter records");

        // The assertion must be updated to reflect the non-optional nature of the fields
        // and the use of proper field names as per the struct definition.
        assert!(filtered_records.iter().all(|record| 
            record.salary >= 70000 && record.salary <= 90000 && record.department == department
        ), "All records should have a salary between 70000 and 90000 in the Finance department.");

        println!("Passed the tests!!!");
    }
}
