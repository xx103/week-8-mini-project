# Week 8 Mini-Project: Employee Data Processing CLI Tool

## Project Description

This command-line interface (CLI) tool is built in Rust for processing employee data. It is capable of ingesting data from a CSV file, applying filters based on department and salary range, and displaying the processed data to the user. It also includes the testing function.

## Project Setup

### Prerequisites

- Rust and Cargo (latest stable version recommended)
- Access to terminal or command line

### Initial Setup

1. Create a new project with `cargo lambda new project_name`.
2. Add `employees.csv` in data directory.
3. Update `main.rs` and `Cargo.toml`.
4. Build the project with `cargo build`.

## Employee Data Structure

The CSV file should follow this structure:

```plaintext
employee_id,name,department,salary,yearswithcompany
1,Employee1,Finance,87194,1
2,Employee2,HR,71962,12
... (more records)
```

- `employee_id`: Unique identifier for the employee (integer)
- `name`: Name of the employee (string)
- `department`: Department in which the employee works (string)
- `salary`: Employee's salary (integer)
- `yearswithcompany`: Number of years the employee has been with the company (integer)

## Functionality

Run the tool using the command below:

```sh
cargo run -- data/employees.csv --department Finance --min_salary 50000 --max_salary 80000
```

- The tool will read the CSV file.
- It will then filter the records to find employees in the 'Finance' department with salaries in the specified range.
- Matching records will be printed to the terminal.

![Function overview](screenshot1.png)

## Testing Implementation

The tool includes a suite of unit tests to verify the functionality of CSV processing and record filtering.

### Sample Test

Here is a sample test that checks if the filtering by department and salary range is functioning as expected:

```rust
#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_salary_filter() {
        let file_path = "data/employees.csv";
        let department = "Finance";
        let min_salary = "70000";
        let max_salary = "90000";

        let records = process_csv(file_path).expect("Failed to process CSV");
        let filtered_records = filter_records(records, department, min_salary, max_salary).expect("Failed to filter records");

        assert!(filtered_records.iter().all(|record| 
            record.salary >= 70000 && record.salary <= 90000 && record.department == department
        ), "Filtered records must match criteria.");
    }
}
```

Run the tests using the following command:

```sh
cargo test
```

Check if the testing succeeds: 
![Function overview](screenshot2.png)
